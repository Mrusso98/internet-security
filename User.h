#include <cstdlib>
#include <string>
#include <ctime>

using namespace std;
struct user {
	int service;
	int flag;
	int src_byte;
	int srv_count;
	int dst_host_count;
	float dst_host_srv_rerror_rate;
	string typeof;
	float concentration_rate;
	bool promoted;

	user() {}
	user(string service, string flag, int src_byte, int srv_count, int dst_host_count, float dst_host_srv_rerror_rate, string typeof) {
		this->service = service_to_int(service);
		this->flag = flag_to_int(flag);
		this->src_byte = src_byte;
		this->srv_count = srv_count;
		this->dst_host_count = dst_host_count;
		this->dst_host_srv_rerror_rate = dst_host_srv_rerror_rate;
		this->typeof = typeof;

		concentration_rate = 3.0f;
		promoted = false;
	}

	user(int service, int flag, int src_byte, int srv_count, int dst_host_count, float dst_host_srv_rerror_rate,string typeof) {
		this->service = service;
		this->flag = flag;
		this->src_byte = src_byte;
		this->srv_count = srv_count;
		this->dst_host_count = dst_host_count;
		this->dst_host_srv_rerror_rate = dst_host_srv_rerror_rate;
		this->typeof = typeof;

		concentration_rate = 3.0f;
		promoted = false;
	}

	void display() {
		cout << "[<" << service << "> <" << flag << "> <" << src_byte <<
			"> <" << srv_count << "> <" << dst_host_count << "> <" << dst_host_srv_rerror_rate << ">]\n";
	}

	int flag_to_int(string flag) {
		if (flag == "OTH") return 0;
		else if (flag == "S1") return 1;
		else if (flag == "S2") return 2;
		else if (flag == "RSTO") return 3;
		else if (flag == "RSTRs") return 4;
		else if (flag == "RSTOS0") return 5;
		else if (flag == "SF") return 6;
		else if (flag == "SH") return 7;
		else if (flag == "REJ") return 8;
		else if (flag == "S0") return 9;
		else if (flag == "S3") return 10;
	}

	int service_to_int(string service) {
		if (service == "other") return 0;
		else if (service == "link") return 1;
		else if (service == "netbios_ssn") return 2;
		else if (service == "smtp") return 3;
		else if (service == "netstat") return 4;
		else if (service == "ctf") return 5;
		else if (service == "ntp_u") return 6;
		else if (service == "harvest") return 7;
		else if (service == "efs") return 8;
		else if (service == "klogin") return 9;
		else if (service == "systat") return 10;
		else if (service == "exec") return 11;
		else if (service == "nntp") return 12;
		else if (service == "pop_3") return 13;
		else if (service == "printer") return 14;
		else if (service == "vmnet") return 15;
		else if (service == "netbios_ns") return 16;
		else if (service == "urh_i") return 17;
		else if (service == "ssh") return 18;
		else if (service == "http_8001") return 19;
		else if (service == "iso_tsap") return 20;
		else if (service == "aol") return 21;
		else if (service == "sql_net") return 22;
		else if (service == "shell") return 23;
		else if (service == "sup_dup") return 24;
		else if (service == "auth") return 25;
		else if (service == "whois") return 26;
		else if (service == "discard") return 27;
		else if (service == "sunrpc") return 28;
		else if (service == "urp_i") return 29;
		else if (service == "Rje") return 30;
		else if (service == "ftp") return 31;
		else if (service == "daytime") return 32;
		else if (service == "domain_u") return 33;
		else if (service == "pm_dump") return 34;
		else if (service == "time") return 35;
		else if (service == "hostnames") return 36;
		else if (service == "name") return 37;
		else if (service == "ecr_i") return 38;
		else if (service == "bgp") return 39;
		else if (service == "telnet") return 40;
		else if (service == "domain") return 41;
		else if (service == "ftp_data") return 42;
		else if (service == "nnssp") return 43;
		else if (service == "courier") return 44;
		else if (service == "finger") return 45;
		else if (service == "uucp_path") return 46;
		else if (service == "X11") return 47;
		else if (service == "imap_4") return 48;
		else if (service == "mtp") return 49;
		else if (service == "login") return 50;
		else if (service == "tftp_u") return 51;
		else if (service == "kshell") return 52;
		else if (service == "private") return 53;
		else if (service == "http_2784") return 54;
		else if (service == "echo") return 55;
		else if (service == "http") return 56;
		else if (service == "ldap") return 57;
		else if (service == "tim_i") return 58;
		else if (service == "netbios_dgm") return 59;
		else if (service == "uucp") return 60;
		else if (service == "eco_i") return 61;
		else if (service == "Remote_job") return 62;
		else if (service == "IRC") return 63;
		else if (service == "http_443") return 64;
		else if (service == "red_i") return 65;
		else if (service == "Z39_50") return 66;
		else if (service == "Pop_2") return 67;
		else if (service == "gopher") return 68;
		else if (service == "Csnet_ns") return 69;
	}
};


bool operator== (user u1, user u2) {
	float service_match =  3 * fabs((u1.service - u2.service) / 70.0);
	float flag_match = 6 * fabs((u1.flag - u2.flag) / 12.0);
	float src_byte_match = fabs((u1.src_byte - u2.src_byte) / 62825648.0);
	float dst_host_count_match = 14 * fabs((u1.dst_host_count - u2.dst_host_count) / 255.0);
	float dst_host_srv_rerror_rate_match = 6 * fabs((u1.dst_host_srv_rerror_rate - u2.dst_host_srv_rerror_rate));
	float srv_count_match = 6 * fabs((u1.srv_count - u2.srv_count)/511.0);

	float matching_rate = (service_match + flag_match + src_byte_match + dst_host_count_match + dst_host_srv_rerror_rate_match + srv_count_match) / 36.0f;
	return matching_rate <= 0.05f;
}

user* createRandom() {
	int service = rand() % 70;
	int flag = rand() % 12;
	int src_byte = rand() % 62825648;
	int dst_host_count = rand() % 255;
	int srv_count = rand() % 511;
	float dst_host_srv_rerror_rate = (float) (rand() % 100)/100.0;
	
	user* u1 = new user(service, flag, src_byte, srv_count, dst_host_count, dst_host_srv_rerror_rate, "normal");
	return u1;
}

user* getUser(ifstream& in) {
	string service, flag, normal;
	int src_byte, srv_count, dst_host_count;
	float dst_host_srv_rerror_rate;
	in >> service >> flag >> src_byte >> srv_count >> dst_host_count >> dst_host_srv_rerror_rate >> normal;
	user* u1;
	if(service != "STOP") u1 = new user(service, flag, src_byte, srv_count, dst_host_count, dst_host_srv_rerror_rate, normal);
	else u1 = new user(service, flag, src_byte, srv_count, dst_host_count, dst_host_srv_rerror_rate, "empty");
	return u1;
}

bool isEqual(user u1, user u2) {
	if (u1.service != u2.service) return false;
	if (u1.flag != u2.flag) return false;
	if (u1.src_byte != u2.src_byte) return false;
	if (u1.srv_count != u2.srv_count) return false;
	if (u1.dst_host_count != u2.dst_host_count) return false;
	if (u1.dst_host_srv_rerror_rate != u2.dst_host_srv_rerror_rate) return false;

	return true;
}
