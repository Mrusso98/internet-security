#include <iostream>
#include <vector>
#include <fstream>
#include "User.h"

using namespace std;

int main() {
	srand(time(0));
	ifstream normal;
	normal.open("normal.txt");
	ifstream all;
	all.open("subset.txt");

	int num_of_selves;
	int num_of_detectors;
	cout << "Inserire numero di user fidati\n";
	cin >> num_of_selves;

	user** self = new user* [num_of_selves]; //definizione unità fidate

	for (int i = 0; i < num_of_selves; i++) {
		self[i] = getUser(normal);
	}

	for (int i = 0; i < num_of_selves; i++) {
		cout << "\nSelf numero " << i << ": ";
		self[i]->display();
	}

	cout << "\nInserire numero di detectors richiesto\n";
	cin >> num_of_detectors;
	vector<user> detectors;
	bool is_stabilized = false;

	while (detectors.size() < num_of_detectors) {
		detectors.push_back(*createRandom());
		for (int i = 0; i < num_of_selves; i++) {
			if (detectors[detectors.size() - 1] == *self[i]) {
				detectors.pop_back();
				break;
			}
		}
	}

	for (unsigned int i = 0; i < detectors.size(); i++) {
		cout << "\nDetector numero " << i << ": ";
		detectors[i].display();
	}

	const int num_of_promoted = 50;
	user promoted_detectors[num_of_promoted];
	int current_index = 0;
	//bool was_found = false; 

	int minacce_rilevate = 0;
	int falsi_positivi = 0;
	int falsi_negativi = 0;
	int pacchetti_sicuri = 0;
	bool in_transaction = true;
	char answer;
	int i = 0;
	cout << "\nIniziare transazione di pacchetti? [Y/N]\n";
	cin >> answer;
	if (answer == 'N') return 0;
	else if (answer == 'Y') {
		while (in_transaction) {
			user antigene = *getUser(all);

			if (i % 1000 == 0) cout << "\nAnalizzati " << i << " pacchetti." << endl;
			i++;

			if (antigene.typeof == "empty") {
				cout << "\nMinacce rilevate: " << minacce_rilevate << "\nFalsi positivi:  " << falsi_positivi <<
					"\nFalsi negativi: " << falsi_negativi << "\nPacchetti sicuri: " << pacchetti_sicuri << endl;
				cout << "Percentuale minacce rilevate: " << (float)minacce_rilevate / (minacce_rilevate + falsi_negativi)*100 << "%" << endl;
				cout << "Pacchetti sicuri rilevati correttamente: " << (float)pacchetti_sicuri / (pacchetti_sicuri + falsi_positivi)*100 << "%" << endl;
				in_transaction = false;
			}

			for (int i = 0; i < detectors.size(); i++) {
				detectors[i].concentration_rate -= 0.1;
				if (detectors[i].concentration_rate <= 0) detectors.erase(detectors.begin() + i);
			}

			while (detectors.size() < num_of_detectors) {
				detectors.push_back(*createRandom());
				for (int i = 0; i < num_of_selves; i++) {
					if (detectors[detectors.size() - 1] == *self[i]) {
						detectors.pop_back();
						break;
					}
				}
			}

			for (unsigned int i = 0; i < detectors.size(); i++) {
				if (antigene == detectors[i]) {
					if (antigene.typeof == "normal") {
						falsi_positivi++;
						detectors.erase(detectors.begin() + i);
					}
					else {
						minacce_rilevate++;
						promoted_detectors[current_index++] = detectors[i];
						detectors.erase(detectors.begin() + i);
						if (current_index == num_of_promoted) current_index = 0;
					}
					break;
				}
				else if (i == detectors.size() - 1) {
					for (int j = 0; j < current_index; j++) {
						if (antigene == promoted_detectors[j]) {
							/*if(isEqual(antigene, *self[0]))
								was_found = true;*/

							if (antigene.typeof == "normal") {
								falsi_positivi++;
							}
							else {
								minacce_rilevate++;
							}
						}
						else if (j == current_index - 1) {
							if (antigene.typeof == "normal")
								pacchetti_sicuri++;
							else falsi_negativi++;
						}
					}
				}
			}
		}
	/*	cout << "\nIl pacchetto ";
		self[0]->display(); 
		if (was_found == true) cout << "e' stato rilevato come minaccia";
		else cout << "non e' stato rilevato come minaccia";*/
	}
}